<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FirmenSpezifisch extends Model
{
    public $keyType = 'string';
    protected $table = 'firmen_spezifisch';
    protected $primaryKey = 'doku_id';

    public function physDatei()
    {
        return $this->hasOne("App\Models\PhysDatei", "doku_id", "doku_id");
    }
}
