<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FirmenSpezifischMulti extends Model
{
    public $keyType = 'string';
    protected $table = 'firm_spez_mult_val';
    protected $primaryKey = 'doku_id';

    public function physDatei()
    {
        return $this->belongsTo("App\Models\PhysDatei", "doku_id", "doku_id");
    }
}
