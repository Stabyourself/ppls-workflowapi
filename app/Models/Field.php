<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $table = 'field_repository';
    protected $primaryKey = 'repository_id';
    public $keyType = 'string';
}
