<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Benutzer extends Model
{
    protected $table = 'benutzer';
    protected $primaryKey = 'benutzername';
    public $keyType = 'string';
}
