<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhysDatei extends Model
{
    public $keyType = 'string';
    protected $table = 'phys_datei';
    protected $primaryKey = 'doku_id';

    public function firmenSpezifisch()
    {
        return $this->hasOne("App\Models\FirmenSpezifisch", "doku_id", "doku_id");
    }

    public function firmenSpezifischMulti()
    {
        return $this->hasMany("App\Models\FirmenSpezifischMulti", "doku_id", "doku_id");
    }

    public function wiederVorlage()
    {
        return $this->belongsTo("App\Models\WiederVorlage", "doku_id", "doku_id");
    }
}
