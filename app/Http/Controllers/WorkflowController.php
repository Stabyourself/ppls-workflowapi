<?php

namespace App\Http\Controllers;

use App\Models\Benutzer;
use App\Models\Field;
use App\Models\PhysDatei;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class WorkflowController extends Controller
{
    const physProperties = [
        "property_last_alteration_date" => "last_update_file",
        "property_editor" => "bearbeiter",
        "property_remark" => "text",
        "property_owner" => "besitzer",
        // "property_caption" => "?",
        "property_filename" => "dateiname",
        "property_filetype" => "datei_erw",
        "property_document_id" => "doku_id",
        "property_document_number" => "zeich_nr",
        "property_creation_date" => "datum_einbring",
        "property_size" => "size_in_byte",
        "property_state" => "logi_verzeichnis",
        "property_variant_number" => "var_nr",
        "property_access_date" => "dat_letzter_zugr",
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validated = $request->validate([
            "repository" => "required",
            "sourcecategory" => "",
            "properties" => "required",
            "user_id" => "",
            "prefilter_column" => "",
            "prefilter_value" => "",
            "type" => "",
            "id" => "",
            "include_outbound" => "",
        ]);

        // SET DB
        Config::set("database.default", $validated["repository"]);
        $connection = config('database.default');

        // DB::enableQueryLog();

        $category_id = $validated["sourcecategory"] ?? null;
        $properties = json_decode($validated["properties"]);

        // filter properties to numericals to find the firmen_spezifisch columns
        $propertiesNumerical = array_filter($properties, function ($property) {
            return intval($property);
        });

        if (!empty($validated["prefilter_column"])) {
            $propertiesNumerical[] = $validated["prefilter_column"];
        }

        // find out column numbers for properties
        $fields = Field::whereIn("repository_id", $propertiesNumerical)->get(["doc_field_nr_pref", "repository_id", "data_type"])->toArray();

        $columnNumbers = [];

        $firmenSpezifischColumns = ["firmen_spezifisch.doku_id"];
        $firmenSpezifischMultiColumns = [];

        foreach ($fields as $field) {
            $columnNumbers[$field["repository_id"]] = $field["doc_field_nr_pref"];

            $columnNumber = $columnNumbers[$field["repository_id"]];
            $i = $field["repository_id"];

            if (is_numeric($columnNumber)) {
                if ($columnNumber < 60 || $columnNumber > 69) {
                    $firmenSpezifischColumns[] = "dok_dat_feld_{$columnNumber}";
                } else {
                    $firmenSpezifischMultiColumns[] = [
                        "actual" => $columnNumber,
                        "requested" => $i,
                        "type" => $field["data_type"],
                    ];
                }
            }
        }

        // dd($firmenSpezifischMultiColumns);

        // get username based on id
        if (intval($validated["type"]) != 0) { // don't need the user on monitor
            $user = Benutzer::where("idp_id", "=", $validated["user_id"])->get(["benutzername", "idp_id"])->first();
            if (!empty($user)) {
                $userName = $user->benutzername;
            } else {
                // This should never happen, but if it does we'll just use an invalid name so that no workflows are returned
                $userName = "invalid";
            }

            // get all groups for this user
            $groups = DB::table("benutzer_in_gruppe")
                ->select("benutzergruppe")
                ->where("benutzername", "=", $userName)
                ->get()
                ->pluck("benutzergruppe");

            $groups->push($userName); // documents can also be owned by a person
        } else {
            $groups = collect();
        }


        // get username based on id
        $senderUser = null;

        if (boolval($validated["include_outbound"] ?? false)) {
            $senderUser = Benutzer::where("idp_id", "=", $validated["user_id"])->get(["benutzername", "idp_id"])->first();

            if (!empty($user)) {
                $senderUser = $user->benutzername;
            } else {
                // This should never happen, but if it does we'll just use an invalid name so that no workflows are returned
                $senderUser = "invalid";
            }
        }

        function filterWiederVorlage($query, $groups, $senderUser)
        {
            $query->where(function ($query) use ($groups, $senderUser) {
                $query->whereIn("benutzername", $groups);

                if ($senderUser) {
                    $query->orWhere("sender", "=", $senderUser);
                }
            });

            // $query->where("wv_typ", "=", "W");
            $query->where(function ($query) {
                $query->whereNull("tstamp_quittiert");
                $query->orWhere("tstamp_quittiert", "=", "");
            });
        }

        $query = PhysDatei::orderBy("datum_einbring", "desc");

        if (!is_null($category_id)) {
            $query->where("dokuart", "=", $category_id);
        }


        // Firmen Spezifisch
        $query->join("firmen_spezifisch", function ($join) use ($validated, $columnNumbers) {
            $join->on("firmen_spezifisch.doku_id", "phys_datei.doku_id");


            // MAUSER SPECIAL FILTERING
            if (intval($validated["type"]) !== 1 && !empty($validated["prefilter_value"])) {
                $join->where("dok_dat_feld_{$columnNumbers[$validated["prefilter_column"]]}", $validated["prefilter_value"]);
            }
        })

            // WV
            ->leftJoin("wieder_vorlage", function ($join) use ($groups, $senderUser) {
                $join->on("wieder_vorlage.doku_id", "=", "phys_datei.doku_id");
                filterWiederVorlage($join, $groups, $senderUser);
            });

        // Firmen Spezifisch Multival
        foreach ($firmenSpezifischMultiColumns as $column) {
            $columnName = "value_char";

            if (in_array($column["type"], ["DATE", "DATETIME"])) {
                $columnName = "value_date";
            }

            if (in_array($column["type"], ["NUM", "MONEY"])) {
                $columnName = "value_num";
            }

            $query->addSelect("t{$column["requested"]}.{$columnName} as {$column["requested"]}");

            $query->leftJoin("firm_spez_mult_val as t{$column["requested"]}", function ($join) use ($column) {
                $join->on("t{$column["requested"]}.doku_id", "=", "phys_datei.doku_id");
                $join->where("t{$column["requested"]}.row_number", "=", 1);
                $join->where("t{$column["requested"]}.field_no", "=", $column["actual"]);
            });
        }


        if (intval($validated["type"]) == 1) {
            $query->whereHas("wiederVorlage", function ($query) use ($groups, $senderUser) {
                filterWiederVorlage($query, $groups, $senderUser);
            });
        }

        if (intval($validated["type"] != 0)) {
            $query->addSelect(["holdfile_id", "wv_typ", "text_wiedervorlage", "benutzername", "sender", "tstamp_gesendet"]);
        }

        $phys_datei_columns = ["property_document_id" => "phys_datei.doku_id"];

        foreach ($properties as $property) {
            if (array_key_exists($property, self::physProperties) && $property != "property_document_id") {
                $phys_datei_columns[$property] = self::physProperties[$property];
            }
        }

        foreach ($columnNumbers as $requested => $actual) {
            if ($actual < 60 || $actual > 69) {
                $query->addSelect("dok_dat_feld_{$actual} as {$requested}");
            }
        }

        foreach ($phys_datei_columns as $requested => $actual) {
            $query->addSelect("{$actual} as {$requested}");
        }

        $query->selectRaw("CONCAT( dok_dat_feld_54, ' ', dok_dat_feld_27) as caption");







        $query->limit(500000); // realistic max

        $documents = $query->get();

        // build the output
        $out = ["data" => []];

        foreach ($documents as $document) {
            $attributes = $document->getAttributes();

            $attributes["_link"] = "/dms/r/{$validated["repository"]}/o2/{$document->property_document_id}";
            $attributes["_update"] = "/dms/r/{$validated["repository"]}/o2/{$document->property_document_id}/update";

            // insert workflow information if present (this is why we're here!)
            if ($document->holdfile_id) {
                $inbound = in_array($document->benutzername, $groups->toArray());

                if ($inbound) {
                    switch ($document->wv_typ) {
                        case "W":
                            $attributes["_workflow"] = "/d3/" . config("database.connections.{$connection}.archive_char") . "/api/v1/redirect/{$document->property_document_id}/doc?holdfile_id={$document->holdfile_id}&recipient={$document->benutzername}";
                            break;

                        default:
                            $attributes["_commentLink"] = "/d3/" . config("database.connections.{$connection}.archive_char") . "/api/v1/redirect/{$document->property_document_id}/doc?holdfile_id={$document->holdfile_id}&recipient={$document->benutzername}";
                            break;
                    }
                }

                $attributes["_comment"] = $document->text_wiedervorlage;

                if (in_array("_workfloweditor", $properties)) {
                    $attributes["_workfloweditor"] = $document->benutzername;
                }

                if (in_array("_workflowsender", $properties)) {
                    $attributes["_workflowsender"] = $document->sender;
                }

                if (in_array("_datereceived", $properties)) {
                    $attributes["_datereceived"] = $document->tstamp_gesendet;
                }

                if (in_array("_inboundoutbound", $properties)) {
                    $attributes["_inboundoutbound"] = $inbound ? "Eingang" : "Ausgang";
                }

                if (in_array("property_caption", $properties)) {
                    $attributes["property_caption"] = $document->caption;
                }
            }
            $out["data"][] = $attributes;
        }

        // dd(DB::getQueryLog());

        return $out;
    }
}
