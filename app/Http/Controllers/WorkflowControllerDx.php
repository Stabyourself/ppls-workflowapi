<?php

namespace App\Http\Controllers;

use App\Models\Benutzer;
use App\Models\Field;
use App\Models\PhysDatei;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

use App\DevExtreme\DbSet;
use App\DevExtreme\DataSourceLoader;

const physProperties = [
    "property_last_alteration_date" => "last_update_file",
    "property_editor" => "bearbeiter",
    "property_remark" => "text",
    "property_owner" => "besitzer",
    // "property_caption" => "?",
    "property_filename" => "dateiname",
    "property_filetype" => "datei_erw",
    "property_document_id" => "doku_id",
    "property_document_number" => "zeich_nr",
    "property_creation_date" => "datum_einbring",
    "property_size" => "size_in_byte",
    "property_state" => "logi_verzeichnis",
    "property_variant_number" => "var_nr",
    "property_access_date" => "dat_letzter_zugr",
];

function utf8ize($mixed)
{
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = utf8ize($value);
        }
    } elseif (is_string($mixed)) {
        return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
    }
    return $mixed;
}

function replaceColumns($mixed, $columns)
{
    if (is_array($mixed)) {
        foreach ($mixed as $key => $value) {
            $mixed[$key] = replaceColumns($value, $columns);
        }
    } elseif (is_string($mixed)) {
        if (array_key_exists($mixed, $columns)) {
            return $columns[$mixed];
        }
        return mb_convert_encoding($mixed, "UTF-8", "UTF-8");
    }
    return $mixed;
}

class WorkflowControllerDx extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // die('http://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);

        $validated = $request->validate([
            "repository" => "required",
            "sourcecategory" => "required",
            "properties" => "required",
            "filter" => "",
            "sort" => "",
            "skip" => "integer|min:0",
            "take" => "integer|min:1",
            "user_id" => "",
            "type" => "",
            "group" => "",
            "requireTotalCount" => ""
        ]);

        $properties = json_decode($validated["properties"]);

        $mySQL = new \mysqli("localhost", "root", "", "ppls-workflowapi");

        $columns = [];

        foreach ($properties as $property) {
            if (array_key_exists($property, physProperties)) {
                $columns[physProperties[$property]] = $property;
            }
        }

        if (isset($validated["requireTotalCount"])) {
            $validated["requireTotalCount"] = $validated["requireTotalCount"] === "true";
        }

        if (isset($validated["sort"])) {
            $validated["sort"] = json_decode($validated["sort"]);
        }

        if (isset($validated["filter"])) {
            $validated["filter"] = json_decode($validated["filter"]);
            $validated["filter"] = replaceColumns($validated["filter"], physProperties);
        }

        if (isset($validated["group"])) {
            $validated["group"] = json_decode($validated["group"]);
            $validated["group"] = replaceColumns($validated["group"], physProperties);
        }

        if (isset($validated["skip"])) {
            $validated["skip"] = intval($validated["skip"]);
        }

        if (isset($validated["take"])) {
            $validated["take"] = intval($validated["take"]);
        }

        $dbSet = new DbSet($mySQL, "phys_datei", $columns);
        $result = DataSourceLoader::Load($dbSet, $validated);

        $result = utf8ize($result);

        return $result;
    }
}
