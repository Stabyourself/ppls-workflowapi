<?php

namespace App\Http\Controllers;

use App\Models\Benutzer;
use App\Models\Field;
use App\Models\PhysDatei;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class WorkflowControllerServerSide extends Controller
{
    const physProperties = [
        "property_last_alteration_date" => "last_update_file",
        "property_editor" => "bearbeiter",
        "property_remark" => "text",
        "property_owner" => "besitzer",
        // "property_caption" => "?",
        "property_filename" => "dateiname",
        "property_filetype" => "datei_erw",
        "property_document_id" => "doku_id",
        "property_document_number" => "zeich_nr",
        "property_creation_date" => "datum_einbring",
        "property_size" => "size_in_byte",
        "property_state" => "logi_verzeichnis",
        "property_variant_number" => "var_nr",
        "property_access_date" => "dat_letzter_zugr",
    ];



    public static function convert_from_latin1_to_utf8_recursively($dat)
    {
        if (is_string($dat)) {
            return utf8_encode($dat);
        } elseif (is_array($dat)) {
            $ret = [];
            foreach ($dat as $i => $d) $ret[$i] = self::convert_from_latin1_to_utf8_recursively($d);

            return $ret;
        } elseif (is_object($dat)) {
            foreach ($dat as $i => $d) $dat->$i = self::convert_from_latin1_to_utf8_recursively($d);

            return $dat;
        } else {
            return $dat;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // die('http://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI']);

        $validated = $request->validate([
            "repository" => "required",
            "sourcecategory" => "required",
            "properties" => "required",
            "filter" => "",
            "sort" => "",
            "skip" => "integer|min:0",
            "take" => "integer|min:1",
            "user_id" => "",
            "type" => "",
            "requireTotalCount" => "",
            "group" => "",
        ]);

        $category_id = $validated["sourcecategory"];
        $properties = json_decode($validated["properties"]);

        if (isset($validated["skip"])) {
            if (!isset($validated["take"])) {
                $validated["take"] = 100;
            }
        }



        // SET DB
        Config::set("database.default", $validated["repository"]);

        $connection = config('database.default');
        $driver = config("database.connections.{$connection}.driver");



        // SET UP VARS

        // filter properties to numericals to find the firmen_spezifisch columns
        $propertiesNumerical = array_filter($properties, function ($property) {
            return intval($property);
        });

        // find out column numbers for properties
        $fields = Field::whereIn("repository_id", $propertiesNumerical)->get(["doc_field_nr_pref", "repository_id"])->toArray();

        $columnNumbers = [];

        foreach ($fields as $field) {
            $columnNumbers[$field["repository_id"]] = $field["doc_field_nr_pref"];
        }

        // build column list
        $firmenSpezifischColumns = ["doku_id"];
        $firmenSpezifischMultiColumns = [];

        foreach ($columnNumbers as $columnNumber) {
            if (is_numeric($columnNumber)) {
                if ($columnNumber < 60 || $columnNumber > 69) {
                    $firmenSpezifischColumns[] = "dok_dat_feld_{$columnNumber}";
                } else {
                    $firmenSpezifischMultiColumns[] = $columnNumber;
                }
            }
        }




        // build properties
        $columnNames = [];

        foreach ($properties as $property) {
            $found = false;
            $multi = false;

            if (intval($property) != 0) {
                // firmen spezifisch
                if (array_key_exists($property, $columnNumbers)) {
                    $columnNumber = $columnNumbers[$property];

                    if ($columnNumber < 60 || $columnNumber > 69) {
                        $column = "[firmen_spezifisch].[dok_dat_feld_{$columnNumber}]";

                        $key = $property;

                        $found = true;
                        // } else {
                        //     $key = $property;

                        //     $i = $document->firmenSpezifischMulti->search(function($item) use ($columnNumber) {
                        //         return ($item->field_no == $columnNumber);
                        //     });

                        //     if ($i !== false) {
                        //         $multi = $document->firmenSpezifischMulti[$i];
                        //         $value = $multi->value_num ?? $multi->value_char ?? $multi->value_date;
                        //     } else {
                        //         $value = null;
                        //     }

                        //     $multi = true;
                        //     $found = true;
                    }
                }
            } else {
                // check if we have a column for what was asked
                if (array_key_exists($property, self::physProperties)) {
                    $column = "[phys_datei].[" . self::physProperties[$property] . "]";

                    $key = $property;

                    $found = true;
                }
            }

            if ($driver == "mysql") {
                $column = str_replace(["[", "]"], "`", $column);
            }

            if ($found) {
                $columnNames[$key] = $column;
            }
        }


        // dd($columnNames);




        // BUILD QUERY

        $query = DB::table("phys_datei");
        $query->select("phys_datei.doku_id as property_document_number");

        // SELECT WHAT WE WANT
        foreach ($columnNames as $property => $column) {
            $query->selectRaw("{$column} as '{$property}'");
        }

        // FILTER BY CATEGORY
        $query->where("dokuart", $category_id);

        // DOK_DAT FIELDS
        $query->join("firmen_spezifisch", "firmen_spezifisch.doku_id", "=", "phys_datei.doku_id");

        // WIEDERVORLAGE
        if (intval($validated["type"]) != 0) { // don't need the user on monitor
            // get username based on id
            $user = Benutzer::where("idp_id", "=", $validated["user_id"])->get(["benutzername", "idp_id"])->first();
            if (!empty($user)) {
                $userName = $user->benutzername;
            } else {
                // This should never happen, but if it does we'll just use an invalid name so that no workflows are returned
                $userName = "invalid";
            }

            // get all groups for this user
            $groups = DB::table("benutzer_in_gruppe")
                ->select("benutzergruppe")
                ->where("benutzername", "=", $userName)
                ->get()
                ->pluck("benutzergruppe");

            $groups->push($userName); // documents can also be owned by a person

            $joinType = "leftJoin";

            if (intval($validated["type"]) == 1) {
                $joinType = "join";
            }

            $query->$joinType("wieder_vorlage", function ($join) use ($groups) {
                $join->on("wieder_vorlage.doku_id", "phys_datei.doku_id");
                $join->whereIn("benutzername", $groups);
                $join->where(function ($join) {
                    $join->whereNull("tstamp_quittiert");
                    $join->orWhere("tstamp_quittiert", "=", "");
                });
            });

            $query->addSelect(["text_wiedervorlage", "holdfile_id", "benutzername"]);
        }


        DB::enableQueryLog();

        // QUERY OPERATIONS

        // SORT
        if (!empty($validated["sort"])) {
            foreach (json_decode($validated["sort"], true) as $sort) {
                $query->orderByRaw($columnNames[$sort["selector"]] . " " . ($sort["desc"] ? "desc" : "asc"));
            }
        } else { // no sorting specified, let's sort by import date
            $query->orderBy("datum_einbring", "desc");
        }

        // SEARCH
        if (!empty($validated["filter"])) {
            $filters = json_decode($validated["filter"]);

            $filterHelper = new \App\DevExtreme\FIlterHelperOwn();

            $filter = $filterHelper->GetSqlExprByArray($filters, $columnNames);
            $query->whereRaw($filter);
        }

        // GROUP
        if (!empty($validated["group"])) { // GROUPING
            $groups = json_decode($validated["group"], true);
            if (gettype($groups[0]) != "array") {
                $groups = [$groups];
            }

            $query->take(100000000);

            $bindings = array_map(function ($el) {
                return '"' . $el . '"';
            }, $query->getBindings());

            $sql = Str::replaceArray('?', $bindings, $query->toSql());

            $query = DB::table(DB::raw("(" . $sql . ") as t1"));
            $query->selectRaw("COUNT(1) as count");
            foreach ($groups as $group) {
                $interval = $group["groupInterval"] ?? "";
                $selector = $group["selector"];

                $asName = "[{$selector}{$interval}]";
                $columnName = strtoupper($interval) . "([{$selector}])";

                $newSelector = "{$columnName} AS {$asName}";

                $query->addSelect(DB::raw($newSelector));
                $query->groupByRaw($columnName);
            }
            $data = [
                "count" => 0,
                "items" => [],
            ];

            // SKIP
            if (isset($validated["skip"])) {
                $validated["skip"] = intval($validated["skip"]);
                $query->skip($validated["skip"]);
            }

            // TAKE
            if (isset($validated["take"])) {
                $validated["take"] = intval($validated["take"]);
                $query->take($validated["take"]);
            }

            $results = $query->get();

            foreach ($results as $result) {
                $current = &$data;

                foreach ($groups as $depth => $group) {
                    $selector = $group["selector"];

                    if (!empty($group["groupInterval"])) {
                        $selector = "{$selector}{$group["groupInterval"]}";
                    }

                    if ($depth < count($groups) - 1) {
                        // find existing array element
                        $found = false;

                        foreach ($current["items"] as $i => $element) {
                            if ($element["key"] == $result->$selector) {
                                $current = &$current["items"][$i];
                                $found = true;
                                break;
                            }
                        }

                        if (!$found) {
                            $current["items"][] = [
                                "key" => $result->$selector,
                                "items" => [],
                                "count" => 0,
                            ];

                            $current = &$current["items"][count($current["items"]) - 1];
                        }
                    } else {
                        $current["count"] += $result->count;
                        array_push($current["items"], [
                            "key" => $result->$selector,
                            "items" => null,
                            "count" => $result->count,
                        ]);
                    }
                }
            }

            return [
                "data" => $data["items"]
            ];
        } else { // REGULAR
            $return = [];

            // COUNT RESULTS
            if (isset($validated["requireTotalCount"])) {
                $return["totalCount"] = $query->count();
            }

            // SKIP
            if (isset($validated["skip"])) {
                $validated["skip"] = intval($validated["skip"]);
                $query->skip($validated["skip"]);
            }

            // TAKE
            if (isset($validated["take"])) {
                $validated["take"] = intval($validated["take"]);
                $query->take($validated["take"]);
            }

            $return["data"] = $query->get();

            // dd(DB::getQueryLog());

            return $return;
        }
    }
}
