FROM php:7.4-fpm

COPY composer.lock composer.json /var/www/


COPY database /var/www/database


WORKDIR /var/www


RUN apt-get update && apt-get -y install git && apt-get -y install zip


RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && php composer.phar install --no-dev --no-scripts \
    && rm composer.phar


COPY . /var/www


RUN chown -R www-data:www-data \
        /var/www/storage \
        /var/www/bootstrap/cache


RUN php artisan cache:clear


RUN php artisan optimize


RUN  apt-get install -y libmcrypt-dev \
        libmagickwand-dev --no-install-recommends \
        && pecl install mcrypt-1.0.3 \
        && docker-php-ext-install pdo_mysql \
        && docker-php-ext-enable mcrypt


# sqlsrv stuff
ENV ACCEPT_EULA=Y

RUN apt-get install -y gnupg2 && apt-get -y install git && apt-get -y install zip

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl https://packages.microsoft.com/config/debian/9/prod.list > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get update \
    && apt-get -y --no-install-recommends install msodbcsql17 unixodbc-dev \
    && pecl install sqlsrv \
    && pecl install pdo_sqlsrv \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

RUN docker-php-ext-enable pdo_sqlsrv
RUN docker-php-ext-enable sqlsrv

RUN mv .env.prod .env


RUN php artisan optimize