SELECT TOP $topCount
                                         a.doku_id as DateiID
                                     , a.dok_dat_feld_17 as KreditorName
                                     , a.dok_dat_feld_12 as KreditorNr
                                     , a.dok_dat_feld_13 as RechnungsNr
                                     , a.dok_dat_feld_72 as RGBrutto
                                     , a.dok_dat_feld_50 as RGDatum
                                     , a.dok_dat_feld_71 as RGNetto
                                     , a.dok_dat_feld_16 as Waehrung
                                     , a.dok_dat_feld_19 as WFBearbeiter
                                     , a.dok_dat_feld_18 as WFStatus
                                     , a.dok_dat_feld_2 as Barcode
                                     , b.datum_einbring as Scandatum
                                     , w.holdfile_id as WFHoldFileId
                                     , w.benutzername as WFPostKorb
                                  FROM firmen_spezifisch a
                            INNER JOIN phys_datei b
                                    ON a.doku_id = b.doku_id
                             LEFT JOIN ( SELECT doku_id
                                              , holdfile_id
                                              , benutzername
                                           FROM wieder_vorlage
                                          WHERE wv_typ = 'W'
                                            AND tstamp_quittiert IS NULL
                                            AND benutzername LIKE ?) w
                                    ON a.doku_id = w.doku_id
                                 WHERE a.kue_dokuart = ?
                                   AND a.doku_id = b.doku_id
                                   AND ISNULL(a.dok_dat_feld_18,'') LIKE  ?
                                   AND ISNULL(a.dok_dat_feld_13,'') LIKE  ?
                                   AND ISNULL(a.dok_dat_feld_12,'') LIKE  ?
                                   AND ISNULL(a.dok_dat_feld_17,'') LIKE  ?
                                   AND ISNULL(a.dok_dat_feld_19,'') LIKE  ?
                                   AND ISNULL(a.dok_dat_feld_16,'') LIKE  ?
                                   AND ( ISNULL(a.dok_dat_feld_17,'') LIKE  ?
                                      OR ISNULL(a.dok_dat_feld_19,'') LIKE  ?
                                      OR ISNULL(a.dok_dat_feld_12,'') LIKE  ?
                                      OR ISNULL(a.dok_dat_feld_13,'') LIKE  ? )
                                   AND $datumsFilter