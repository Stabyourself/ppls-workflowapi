<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/workflow', [App\Http\Controllers\WorkflowController::class, 'index']);
Route::get('/workflowserverside', [App\Http\Controllers\WorkflowControllerServerSide::class, 'index']);
Route::get('/workflowdx', [App\Http\Controllers\WorkflowControllerDx::class, 'index']);
