<?php

return [
    "archive_char" => env("D3_ARCHIVE_CHAR", "T"),
    "repository" => env("D3_REPOSITORY", ""),
];
